import { expect} from '@playwright/test';

export class GlobalPage{

    constructor(page){
        this.page = page
        this.searchTextbox = page.locator('xpath=//a[contains(@class, "search-field")]')
    }

    async gotoAndVerify(url, pageTitle){
        await this.page.goto(url);
        await expect(this.page).toHaveTitle(pageTitle);
    }
}
