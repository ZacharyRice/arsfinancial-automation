import { expect} from '@playwright/test';

export class searchPage{

    constructor(page){
        this.page = page
        this.searchTextbox = page.locator('xpath=//a[contains(@class, "search-field")]')
        this.searchButton = page.locator('xpath=//*[@class="search-submit"]')
        this.searchNextPageButton = page.locator('xpath=//a[contains(@class, "next page-numbers")]')
        this.searchArticleTitle = page.locator('xpath=//*[@id="archive-container"]/article/div/header/h2/a')
    }

    async gotoAndVerify(url, pageTitle){
        await this.page.goto(url);
        await expect(this.page).toHaveTitle(pageTitle);
    }

    async emptySearch(){
        await expect(this.searchButton).toBeVisible();
        await this.searchButton.click();
        await expect(this.page).toHaveTitle(/You searched for/);
    }
}
