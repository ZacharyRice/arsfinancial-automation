import { test, expect } from '@playwright/test';
import { GlobalPage } from "../pages/globalPage";

const pageTitle = 'ARS – Income Innovation that Challenges Traditional Thinking';

test('healthcheck for for https://arsfinancial.com/', async ({ page }) => {

    const pageObjects = new GlobalPage(page);
    await pageObjects.gotoAndVerify('https://arsfinancial.com/', pageTitle);
});

test('healthcheck for for https://thelifetimeincomebuilder.com/', async ({ page }) => {

    const pageObjects = new GlobalPage(page);
    await pageObjects.gotoAndVerify('https://thelifetimeincomebuilder.com/', pageTitle);
});

test('healthcheck for for https://www.annexusretirementsolutions.com/', async ({ page }) => {

    const pageObjects = new GlobalPage(page);
    await pageObjects.gotoAndVerify('https://www.annexusretirementsolutions.com/', pageTitle);
});

