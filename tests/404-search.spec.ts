import { test, expect } from '@playwright/test';
import { searchPage } from "../pages/searchPage";
let nextPage = true;

test('search results from the 404 page', async ({ page }) => {

  const searchControls = new searchPage(page);

  await searchControls.gotoAndVerify('https://arsfinancial.com/badurl000000', /Page not found/);

  await searchControls.emptySearch();

  await expect(searchControls.searchNextPageButton).toBeVisible();

  while(nextPage) {
    for (const articleTitle of await searchControls.searchArticleTitle.allInnerTexts()){
      await expect(articleTitle.toLowerCase()).not.toContain('test');
      if(articleTitle.toLowerCase() == 'news') {
        nextPage = false;
      }
    }
    if(nextPage) {
      await searchControls.searchNextPageButton.click();
    }
  }
});

